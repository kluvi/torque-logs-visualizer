<script type="text/javascript" src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="http://code.highcharts.com/stock/highstock.js"></script>
<script type="text/javascript" src="http://code.highcharts.com/modules/exporting.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>





<?php
date_default_timezone_set('Europe/Prague');

if(isset($_GET['map']))
{
	echo '<div id="map-canvas" style="width: 29%; height: 100%; float: right"></div>
<div id="container" style="width: 70%; height: 100%; float: left;"></div>';
}
else
{
	echo '<div id="container" style="width: 100%; height: 100%; margin: 0 auto;"></div>';
}

$file = 'logs/trackLog-2014-srp-12_07-01-00-small.csv';
$file = 'logs/trackLog-2014-srp-12_07-01-00.csv';

echo "<div style=\"clear:both\"></div>
<p>
	<strong>ATF:</strong> Automatic transmission fluid
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<strong>EGR:</strong> Exhaust Gas Recirculation
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<strong>VGT:</strong> Variable-geometry turbocharger
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
echo "<a href=\"{$file}\">stáhnout CSV s daty</a>";
echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
echo "<a href=\"?map\">zobrazit s mapu</a>";
echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
echo "<a href=\"?\">zobrazit bez mapy</a>";
echo "</p>";

$limitTime = array(
	'from' => '2014-08-12 7:15',
	'to' => '2014-08-12 7:35'
);

$flags = array(
	'2014-08-12 07:28' => 'Začalo blikat (cca 7:28)',
);

$translations = array(

);

$hideColsCustom = array(
	'Fuel Rail Pressure(kpa)', // je duplicitne v [KADV]
	'Voltage (OBD Adapter)(V)',
	'Horsepower (At the wheels)(hp)',
	'Barometric pressure (from vehicle)(kpa)',
	
);

$hideColsTime = array(
	'GPS Time',
//	'Device Time', // musi byt vzdycky!!!
);

$hideColsGPS = array(
//	'Longitude',
//	'Latitude',
	'Horizontal Dilution of Precision',
	'Altitude',
	'Bearing',
	'GPS Latitude(°)',
	'GPS Longitude(°)',
	'GPS Altitude(m)',
	'GPS Bearing(°)',
	'GPS Accuracy(m)',
	'GPS Satellites',
);

$hideColsGSensors = array(
	'G(x)',
	'G(y)',
	'G(z)',
	'G(calibrated)',
	'Acceleration Sensor(Z axis)(g)',
	'Acceleration Sensor(X axis)(g)',
	'Acceleration Sensor(Y axis)(g)',
	'Acceleration Sensor(Total)(g)',
);

$hideColsFuel = array(
	'Litres Per 100 Kilometer(Long Term Average)(l/100km)',
	'Litres Per 100 Kilometer(Instant)(l/100km)',
	'Trip average Litres/100 KM(l/100km)',
	'Trip average KPL(kpl)',
	'Trip average Litres/100 KM(l/100km)',
	'Kilometers Per Litre(Long Term Average)(kpl)',
	'Cost per mile/km (Instant)(Kč/km)',
	'Cost per mile/km (Trip)(Kč/km)',
	'Fuel Remaining (Calculated from vehicle profile)(%)',
	'Kilometers Per Litre(Instant)(kpl)',
	'Miles Per Gallon(Long Term Average)(mpg)',
	'Miles Per Gallon(Instant)(mpg)',
	'Fuel cost (trip)(cost)',
	'Trip average MPG(mpg)',
	'Fuel used (trip)(l)',
);

$hideColsTimes = array(
	'Trip Time(Since journey start)(s)',
	'Trip time(whilst stationary)(s)',
	'Trip time(whilst moving)(s)',
	'Run time since engine start(s)',
);

$hideColsDistances = array(
	'Trip Distance(km)',
	'Distance to empty (Estimated)(km)',
	'Trip distance (stored in vehicle profile)(km)',
	'Distance travelled since codes cleared(km)',
);

$hideColsSpeed = array(
//	'Speed (GPS)(km/h)',
	'GPS Speed (Meters/second)',
	'GPS vs OBD Speed difference(km/h)',
	'GPS Speed (Meters/second)',
	'Speed (OBD)(km/h)',

	'0-30mph Time(s)',
	'100-0kph Time(s)',
	'60-0mph Time(s)',
	'1/4 mile time(s)',
	'40-60mph Time(s)',
	'60-80mph Time(s)',
	'80-120kph Time(s)',

	'Average trip speed(whilst moving only)(km/h)',
	'Average trip speed(whilst stopped or moving)(km/h)',
);

$hideCols = array();
$hideCols = array_merge($hideCols, $hideColsCustom);
$hideCols = array_merge($hideCols, $hideColsTime);
$hideCols = array_merge($hideCols, $hideColsGPS);
$hideCols = array_merge($hideCols, $hideColsGSensors);
$hideCols = array_merge($hideCols, $hideColsFuel);
$hideCols = array_merge($hideCols, $hideColsTimes);
$hideCols = array_merge($hideCols, $hideColsDistances);
$hideCols = array_merge($hideCols, $hideColsSpeed);

//var_dump($hideCols);

$data = array();
$f = fopen($file,'r');
$i = 0;
$headers = array();
$colsFilled = array();

while($row = fgets($f))
{
	$i++;

	// rozdelim radek podle carek na sloupecky
	$cols = explode(',', $row);

	// orezu mezery
	foreach($cols as $k=>$v)
		$cols[$k] = trim($v);

	// kdyz je to prvni radek, tak ho pouziju jako klice pro dalsi radky
	if(count($headers) == 0 && $i == 1)
	{
		$headers = $cols;
		continue;
	}

	// vytvorim z toho asociativni pole
	$rowArray = array();
	foreach($cols as $k => $v)
	{
		if($headers[$k] != $v)
			$rowArray[$headers[$k]] = $v;
	}

	if(count($rowArray) == 0)
		continue;

	// zjistim pocet radku s datama v jednotlivejch sloupeckach
	foreach($rowArray as $k=>$v)
	{
		if(!array_key_exists($k, $colsFilled))
			$colsFilled[$k] = 0;

		if($v != '-' && $v != $k)
			$colsFilled[$k]++;
	}

	$data[] = $rowArray;
}
if(!feof($f))
	die('Chyba pri cteni souboru.');
//var_export($data);die;
// odstranim z pole sloupecky co sou vsude prazdne
foreach($colsFilled as $k=>$v)
{
	if($v > 0)
		continue;

	foreach($data as $kk=>$vv)
	{
		foreach($vv as $kkk => $vvv)
		{
			if($kkk == $k)
				unset($data[$kk][$kkk]);
		}
	}
}
//var_dump($data);
//var_dump($colsFilled);

// odstranim sloupecky kde je celou dobu stejna hodnota
$sameValues = array();
$sameValuesDelete = array();
foreach($data as $row)
{
	foreach($row as $name=>$value)
	{
		if($value == '-')
			continue;
		
		$sameValues[$name][$value] = $value;
	}
}

foreach($sameValues as $name => $vals)
{
	if(count($vals) == 1)
		$sameValuesDelete[$name] = true;
}

foreach($sameValuesDelete as $k=>$v)
{
	foreach($data as $kk=>$vv)
	{
		foreach($vv as $kkk => $vvv)
		{
			if($kkk == $k)
				unset($data[$kk][$kkk]);
		}
	}

	unset($colsFilled[$k]);
}

unset($colsFilled['Device Time']);

//var_dump($hideCols);

// odstranim sloupecky co me nezajimaji
foreach($hideCols as $what)
{
	foreach($data as $k=>$v)
	{
		unset($data[$k][$what]);
	}

	unset($colsFilled[$what]);
}

//$maxValues = array();
//$minValues = array();
//
//foreach($data as $k=>$row)
//{
//	foreach($row as $name=>$value)
//	{
//		if($value == '-')
//			continue;
//
//		if(!array_key_exists($name, $maxValues))
//			$maxValues[$name] = ceil($value);
//
//		if(!array_key_exists($name, $minValues))
//			$minValues[$name] = floor($value);
//
//		if($maxValues[$name] < $value)
//			$maxValues[$name] = ceil($value);
//
//		if($minValues[$name] > $value)
//			$minValues[$name] = floor($value);
//	}
//}

ksort($colsFilled);

//var_dump($sameValuesDelete);
//
//var_dump($data);
// vypisu si jen sloupecek co chcu
//$showCol = '[KADV] ATF Temperature(°C)';
//foreach($data as $row)
//{
//	if(isset($row[$showCol]) && $row[$showCol] != '-')
//		var_dump($row[$showCol]);
//}
?>
<script type="text/javascript">
$(document).ready(function(){
	$('#container').highcharts(/*'StockChart',*/ {
		chart: {
			type: 'spline'
		},
        title: {
            text: '<?php echo basename($file); ?>',
            x: -20 //center
        },
        /*yAxis: [
		<?php
			foreach($colsFilled as $k => $v)
			{
				if($v == 0)
					continue;

				$what = $k;
				if($what == 'Device Time' || in_array($what, $hideCols))
					continue;

				if($what == 'Longitude' || $what == 'Latitude')
					continue;

				$kTrans = $k;
				if(array_key_exists($k, $translations))
					$kTrans = $translations[$k];
				echo "{\n";
				echo "title: { text: null },\n";
//				echo "min: {$minValues[$k]},\n";
				echo "max: {$maxValues[$k]},\n";
				echo "labels: { enabled: false }";
				echo "},\n";
			}
		?>
		],*/
		xAxis: {
			type: 'datetime',
			dateTimeLabelFormats: {
                minute: '%H:%M:%S'
            }
		},
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            borderWidth: 1
        },
        series: [
		<?php
			foreach($colsFilled as $k => $v)
			{
				if($v == 0)
					continue;

				$kTrans = $k;
				if(array_key_exists($k, $translations))
					$kTrans = $translations[$k];

				if($kTrans == 'Device Time' || in_array($kTrans, $hideCols))
					continue;

				if($kTrans == 'Longitude' || $kTrans == 'Latitude')
					continue;

				echo "{\n";
				echo "type: 'spline',\n";
				echo "name: '{$kTrans}',\n";
				echo "turboThreshold: 100000,\n";
				echo "data: [\n";
				foreach($data as $row)
				{
					foreach($row as $what=>$value)
					{
						if($what != $k)
							continue;

						if($value == '-')
							continue;

						if($value == '∞')
							$value = 9999;

						if($what == 'Device Time' || in_array($what, $hideCols))
							continue;

						if($what == 'Longitude' || $what == 'Latitude')
							continue;

						$parsedTime = my_parseTIme($row['Device Time']);

						if(isset($limitTime['from']) && $limitTime['from'] != '' && $parsedTime < strtotime($limitTime['from'])*1000)
							continue;

						if(isset($limitTime['to']) && $limitTime['to'] != '' && $parsedTime > strtotime($limitTime['to'])*1000)
							continue;

						echo "{\n";
						echo "x: $parsedTime,\n";
						echo "y: {$value},\n";
						echo "marker: {
							enabled: false
						}\n";
						echo "},\n";
					}
				}
				echo "]\n },\n";
			}

			if(isset($flags) && count($flags) > 0)
			{
				echo "{\n";
				echo "type: 'flags',\n";
				echo "name: 'Poznámky',\n";
				echo "data: [";
				foreach($flags as $time => $flag)
				{
					$time = strtotime($time)*1000;
					echo "{\n";
					echo "x: {$time},\n";
					echo "title: '{$flag}'\n";
					echo "},\n";
				}
				echo "],";
				echo "shape: 'squarepin'\n";
				echo "}\n";
			}
		?>
	]
    });

<?php
if(isset($_GET['map']))
{
			echo "var mapOptions = {
    zoom: 11,
    center: new google.maps.LatLng(49.1520736,16.4435166,12)
  }
  var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);";

			echo "var flightPath = new google.maps.Polyline({
    path: [";
			
			foreach($colsFilled as $k => $v)
			{
				if($v == 0)
					continue;

				$kTrans = $k;
				if(array_key_exists($k, $translations))
					$kTrans = $translations[$k];

				foreach($data as $row)
				{
					$parsedTime = my_parseTIme($row['Device Time']);
					
//					echo "var marker_{$parsedTime} = new google.maps.Marker({
//						position: new google.maps.LatLng({$row['Latitude']},{$row['Longitude']}),
//						map: map,
//						title: '{$parsedTime}',
//						icon: {
//							path: google.maps.SymbolPath.CIRCLE,
//							scale: 1
//						}
//					});\n";
					echo "new google.maps.LatLng({$row['Latitude']},{$row['Longitude']}),";
				}
			}

			echo "],
    geodesic: true,
    strokeColor: '#FF0000',
    strokeOpacity: 1.0,
    strokeWeight: 2
  });
flightPath.setMap(map);
";
}
?>
});
</script>
























<?php
function my_parseTIme($time)
{
	$months = array(
		'led' => 1,
		'srp' => 8,
	);
	$timeA = array(
		'day' => $time[0].$time[1],
		'month' => $months[$time[3].$time[4].$time[5]],
		'year' => $time[7].$time[8].$time[9].$time[10],
		'hour' => $time[12].$time[13],
		'minute' => $time[15].$time[16],
		'second' => $time[18].$time[19],
		'milisecond' => $time[21].$time[22].$time[23]
	);
	$timestamp = mktime($timeA['hour'], $timeA['minute'], $timeA['second'], $timeA['month'], $timeA['day'], $timeA['year']).''.$timeA['milisecond'];

	$time = $timestamp;
//	$time = date('Y-m-d H:i:s');
	return $time;
}